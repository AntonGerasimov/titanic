import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+)\.', expand=False)

    age_median = df.groupby('Title')['Age'].median()

    result = []

    for title in ['Mr', 'Mrs', 'Miss']:
        num_missing = df.loc[df['Title'] == title, 'Age'].isnull().sum()
        median_value = round(age_median[title])
        result.append((title+'.', num_missing, median_value))

    return result